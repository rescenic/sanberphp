<?php

function geser_huruf($ch, $key)
{
	if (!ctype_alpha($ch))
		return $ch;

	$offset = ord(ctype_upper($ch) ? 'A' : 'a');
	return chr(fmod(((ord($ch) + $key) - $offset), 26) + $offset);
}

function ubah_huruf($input, $key)
{
	$output = "";

	$inputArr = str_split($input);
	foreach ($inputArr as $ch)
		$output .= geser_huruf($ch, $key);

	return $output;
}

function huruf_asal($input, $key)
{
	return ubah_huruf($input, 26 - $key);
}

$text1 = "wow";
$text2 = "developer";
$text3 = "laravel";
$text4 = "keren";
$text5 = "semangat";

$cipherText1 = ubah_huruf($text1, 1);
$plainText1 = huruf_asal($cipherText1, 1);

$cipherText2 = ubah_huruf($text2, 1);
$plainText2 = huruf_asal($cipherText2, 1);

$cipherText3 = ubah_huruf($text3, 1);
$plainText3 = huruf_asal($cipherText3, 1);

$cipherText4 = ubah_huruf($text4, 1);
$plainText4 = huruf_asal($cipherText4, 1);

$cipherText5 = ubah_huruf($text5, 1);
$plainText5 = huruf_asal($cipherText5, 1);

echo "Kalimat Awal: " . $plainText1 . "<br>";
echo "Kalimat Huruf Digeser: " . $cipherText1 . "<br><br>";

echo "Kalimat Awal: " . $plainText2 . "<br>";
echo "Kalimat Huruf Digeser: " . $cipherText2 . "<br><br>";

echo "Kalimat Awal: " . $plainText3 . "<br>";
echo "Kalimat Huruf Digeser: " . $cipherText3 . "<br><br>";

echo "Kalimat Awal: " . $plainText4 . "<br>";
echo "Kalimat Huruf Digeser: " . $cipherText4 . "<br><br>";

echo "Kalimat Awal: " . $plainText5 . "<br>";
echo "Kalimat Huruf Digeser: " . $cipherText5 . "<br><br>";

?>